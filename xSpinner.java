package scripts.xSpinner;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;

import scripts.xSpinner.Nodes.Antiban;
import scripts.xSpinner.Nodes.BankFlax;
import scripts.xSpinner.Nodes.BankToSpinner;
import scripts.xSpinner.Nodes.CheckIfWeAreStillSpinning;
import scripts.xSpinner.Nodes.RunToSpot;
import scripts.xSpinner.Nodes.SpinFlax;
import scripts.xSpinner.Nodes.SpinnerToBank;
import scripts.xSpinner.User.Variables;
import scripts.xSpinner.Utils.Node;

@ScriptManifest(authors = { "xCode" }, category = "Crafting", name = "xSpinner")
public class xSpinner extends Script implements Painting{

	private static final long START_TIME = Timing.currentTimeMillis();
	private ArrayList<Node> nodes = new ArrayList<Node>();
	private int startLevel = Skills.getActualLevel(SKILLS.CRAFTING);
	private int startXP = Skills.getXP(SKILLS.CRAFTING);
	
	private final Color TRANSP_BLACK_COLOR = new Color(0, 0, 0, 170);
	private final Color TRANSP_GREEN_COLOR = new Color(99, 209, 62, 170);
	private final Font TEXT_FONT = new Font("Arial", 0, 13);
	private final Font TEXT_ITALIC_FONT = new Font("Arial", 2, 11);
	private final Font TITLE_FONT = new Font("Arial", 0, 18);
	
	@Override
	public void run() {
		init();
		
		while(!Variables.stop)
		{
			for(Node n : nodes)
			{
				if(n.isValid())
				{
					Variables.status = n.toString();
					n.execute();
				}
			}
			General.sleep(30, 100);
		}
	}

	@Override
	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D)gg;
		int gainedXP = Skills.getXP(SKILLS.CRAFTING) - startXP;
		int levelsGained = Skills.getActualLevel(SKILLS.CRAFTING) - startLevel;
		long runTimeLong = Timing.currentTimeMillis() - START_TIME;
		String runTime = Timing.msToString(runTimeLong);
		int spins = gainedXP / 15;
		double spinsPH = ((double)spins * (3600000.0 / runTimeLong));
		double xpPH = ((double)gainedXP * (3600000.0 / runTimeLong));
		double levelsPH = ((double)levelsGained * (3600000.0 / runTimeLong));
		long ttnl = (long)(Skills.getXPToNextLevel(SKILLS.CRAFTING) / xpPH * 3600000);
		
		int paintX = 30;
		int paintY = 283;
		int row2offsetX = 290;
		int percentToNextLevel = Skills.getPercentToNextLevel(SKILLS.CRAFTING);
		int greenBarWidth = (int) (513.0 * (percentToNextLevel / 100.0));
		
		g.setColor(TRANSP_BLACK_COLOR);
		g.fillRect(3, 246, 513, 93);
		
		g.setColor(Color.white);
		
		g.setFont(TITLE_FONT);
		g.drawString("xSpinner", 205, 265);
		
		g.setFont(TEXT_ITALIC_FONT);
		g.drawString("by xCode", 215, 280);
		
		g.setFont(TEXT_FONT);
		
		g.drawString("Runtime: " + runTime, 185, 317);
		g.drawString("Flax spinned: " + spins + " [" + (int)spinsPH + " P/H]", paintX, paintY);
		g.drawString("Status: " + Variables.status, paintX, paintY + 15);
		
		g.drawString("XP Gained: " + gainedXP + " [" + (int)xpPH + " P/H]", row2offsetX, paintY);
		g.drawString("Levels Gained: " + levelsGained + " [" + (int)levelsPH + " P/H]", row2offsetX, paintY + 15);
		
		g.setColor(TRANSP_GREEN_COLOR);
		
		g.fillRect(4, 322, greenBarWidth, 16);
		g.setColor(Color.DARK_GRAY);
		g.drawLine(4, 322, 517, 322);
		g.setColor(Color.WHITE);
		
		if(gainedXP > 0)
			g.drawString(percentToNextLevel + "% to " + (Skills.getActualLevel(SKILLS.CRAFTING) + 1) + " Crafting [" + Timing.msToString(ttnl) + "]", 155, 335);
		
	}
	
	private void init()
	{
		General.useAntiBanCompliance(true);
		nodes.add(new BankFlax());
		nodes.add(new BankToSpinner());
		nodes.add(new SpinFlax());
		nodes.add(new SpinnerToBank());
		nodes.add(new CheckIfWeAreStillSpinning());
		nodes.add(new RunToSpot());
		nodes.add(new Antiban());
	}

}
