package scripts.xSpinner.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
import org.tribot.api2007.util.DPathNavigator;

import scripts.xSpinner.Utils.Node;

public class SpinnerToBank extends Node{

	DPathNavigator dpn = new DPathNavigator();
	
	@Override
	public boolean isValid() {
		return (!Banking.isInBank() && Inventory.find("Flax").length == 0) || Player.getRSPlayer().isInCombat();
	}

	@Override
	public void execute() {
		if(Player.getPosition().getPlane() == 1)
		{
			dpn.setStoppingConditionCheckDelay(General.random(3000,4000));
			dpn.setStoppingCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(40,60);
					return Objects.findNearest(5, "Staircase").length > 0;
				}
			});
			if(dpn.traverse(new RSTile(3205, 3209, 1)) || Objects.findNearest(5, "Staircase").length > 0)
			{
				RSObject[] stairs = Objects.findNearest(5, "Staircase");
				if(stairs.length > 0)
				{
					if(DynamicClicking.clickRSObject(stairs[0], "Climb-up"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(40,60);
								return Player.getPosition().getPlane() == 2;
							}
						}, General.random(1200, 1600));
					}
				}
			}
		}
		if(Player.getPosition().getPlane() == 2)
		{
			dpn.setStoppingCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(40,60);
					return Banking.isInBank();
				}
			});
			dpn.traverse(new RSTile(3208, 3220, 2));
		}
	}
	
	@Override
	public final String toString()
	{
		return "Going to bank";
	}

}
