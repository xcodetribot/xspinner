package scripts.xSpinner.Nodes;

import org.tribot.api.Timing;
import org.tribot.api2007.Player;

import scripts.xSpinner.User.Variables;
import scripts.xSpinner.Utils.Node;

public class CheckIfWeAreStillSpinning extends Node{

	@Override
	public boolean isValid() {
		return Player.getAnimation() != -1;
	}

	@Override
	public void execute() {
		Variables.lastTimeSpinned = Timing.currentTimeMillis();
	}

	@Override
	public final String toString()
	{
		return "Spinning";
	}
}
