package scripts.xSpinner.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;

import scripts.xSpinner.Utils.Node;

public class BankFlax extends Node{

	@Override
	public boolean isValid() {
		return Inventory.getCount("Flax") == 0 && Player.getPosition().getPlane() == 2 && Banking.isInBank();
	}

	@Override
	public void execute() {
		if(!Banking.isBankScreenOpen())
		{
			Banking.openBank();
		}
		if(Banking.isBankScreenOpen())
		{
			if(Inventory.getAll().length > 0)
			{
				Banking.depositAll();
Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(40,100);
						return Inventory.getAll().length == 0;
					}
				}, General.random(800, 1200));
			}
			if(Inventory.getAll().length == 0)
			{
				Banking.withdraw(0, "Flax");
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(40,100);
						return Inventory.getCount("Flax") > 0;
					}
				}, General.random(800, 1200));
			}
		}
	}
	
	@Override
	public final String toString()
	{
		return "Banking";
	}

}
