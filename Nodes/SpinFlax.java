package scripts.xSpinner.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Keyboard;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.xSpinner.User.Variables;
import scripts.xSpinner.Utils.Node;

public class SpinFlax extends Node{

	@Override
	public boolean isValid() {
		return Player.getPosition().getPlane() == 1 && Objects.findNearest(8, "Spinning wheel").length > 0 && Inventory.getCount("Flax") > 0 && Player.getAnimation() == -1 && (Timing.currentTimeMillis() - Variables.lastTimeSpinned >= 3000) && PathFinding.canReach(new RSTile(3209, 3213, 1), false) && !Player.getRSPlayer().isInCombat();
	}

	@Override
	public void execute() {
		if(!Interfaces.isInterfaceValid(459))
		{
			RSObject[] spinningwheel = Objects.findNearest(8, "Spinning wheel");
			if(spinningwheel.length > 0)
			{
				if(spinningwheel[0].isOnScreen())
				{
					if(DynamicClicking.clickRSObject(spinningwheel[0], "Spin"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(50, 100);
								return Interfaces.isInterfaceValid(459);
							}
						}, General.random(3000, 4000));
					}
				}
				else
				{
					Camera.turnToTile(new RSTile(3209, 3213, 1));
				}
			}
		}
		if(Interfaces.isInterfaceValid(459))
		{
			RSInterfaceChild bowStringChild = Interfaces.get(459, 91);
			if(bowStringChild.click("Make X"))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.random(40,100);
						return Interfaces.isInterfaceValid(548) && Interfaces.get(548, 119) != null && !Interfaces.get(548, 119).isHidden();
					}
				}, General.random(600, 1000));
				
				if(Interfaces.isInterfaceValid(548) && Interfaces.get(548, 119) != null && !Interfaces.get(548, 119).isHidden())
				{
					Keyboard.typeSend(General.random(28, 99) + "");
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(40,100);
							return Player.getAnimation() != -1;
						}
					}, General.random(700, 800));
				}
			}
		}
	}

	@Override
	public final String toString()
	{
		return "Attempting to spin";
	}
}
