package scripts.xSpinner.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;

import scripts.xSpinner.Utils.Node;

public class RunToSpot extends Node{

	private final RSTile stairsSpot = new RSTile(3206, 3209, 0);
	
	@Override
	public boolean isValid() {
		return Player.getPosition().getPlane() == 0;
	}

	@Override
	public void execute() {
		WebWalking.walkTo(stairsSpot, new Condition() {
			
			@Override
			public boolean active() {
				General.sleep(40, 80);
				return Player.getPosition().distanceTo(stairsSpot) <= 5 && Objects.findNearest(5, "Staircase").length > 0;
			}
		}, General.random(3000, 4000));
		
		RSObject[] stairs = Objects.findNearest(5, "Staircase");
		if(stairs.length > 0)
		{
			if(stairs[0].isOnScreen())
			{
				if(DynamicClicking.clickRSObject(stairs[0], "Climb-up"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(40,60);
							return Player.getPosition().getPlane() == 1;
						}
					}, General.random(800, 1200));
				}
			}
			else
			{
				Camera.turnToTile(stairs[0].getPosition());
			}
		}
	}
	
	@Override
	public final String toString()
	{
		return "We gotta go back";
	}

}
