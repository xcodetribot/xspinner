package scripts.xSpinner.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;
import org.tribot.api2007.util.DPathNavigator;

import scripts.xSpinner.Utils.Node;

public class BankToSpinner extends Node{

	DPathNavigator dpn = new DPathNavigator();
	private final Positionable spinningWheelPosition = new RSTile(3209, 3213, 1);
	
	@Override
	public boolean isValid() {
		return Objects.findNearest(1, "Spinning wheel").length == 0 && Inventory.getCount("Flax") > 0;
	}

	@Override
	public void execute() {
		if(Player.getPosition().getPlane() == 2)
		{
			dpn.setStoppingConditionCheckDelay(General.random(3000,4000));
			dpn.setStoppingCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(40,60);
					return Objects.findNearest(5, "Staircase").length > 0;
				}
			});
			if(dpn.traverse(new RSTile(3205, 3209, 2)) || Objects.findNearest(5, "Staircase").length > 0)
			{
				RSObject[] stairs = Objects.findNearest(5, "Staircase");
				if(stairs.length > 0)
				{
					if(Player.getPosition().getPlane() == 2 && DynamicClicking.clickRSObject(stairs[0], "Climb-down"))
					{
						Timing.waitCondition(new Condition() {
							
							@Override
							public boolean active() {
								General.sleep(40,60);
								return Player.getPosition().getPlane() == 1;
							}
						}, General.random(2000, 2500));
					}
				}
			}
		}
		if(Player.getPosition().getPlane() == 1)
		{
			dpn.setStoppingCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(40,60);
					RSObject[] spinningWheel = Objects.findNearest(8, "Spinning wheel");
					return spinningWheel.length > 0 && PathFinding.canReach(spinningWheelPosition, false) && spinningWheel[0].isOnScreen();
				}
			});
			dpn.traverse(spinningWheelPosition);
		}
	}
	
	@Override
	public final String toString()
	{
		return "To Spinning wheel";
	}

}
