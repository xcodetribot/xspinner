package scripts.xSpinner.Nodes;

import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;

import scripts.xSpinner.User.Variables;
import scripts.xSpinner.Utils.Node;

public class Antiban extends Node{

	private final ABCUtil antiban = new ABCUtil();
	@Override
	public boolean isValid() {
		return Player.getAnimation() != -1;
	}

	@Override
	public void execute() {
		antiban.performEquipmentCheck();

		antiban.performExamineObject();

		antiban.performFriendsCheck();

		antiban.performLeaveGame();

		antiban.performMusicCheck();

		antiban.performPickupMouse();

		antiban.performQuestsCheck();

		antiban.performRandomMouseMovement();

		antiban.performRandomRightClick();

		antiban.performRotateCamera();
		
		antiban.performTimedActions(SKILLS.CRAFTING);
	}
	
	@Override
	public String toString()
	{
		return Variables.status;
	}

}
