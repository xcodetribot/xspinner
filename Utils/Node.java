package scripts.xSpinner.Utils;

public abstract class Node {
	/**
     * Is action valid?
     * @return 
     */
    public abstract boolean isValid();
    
    /**
     * Execute action.
     */
    public abstract void execute();
}
